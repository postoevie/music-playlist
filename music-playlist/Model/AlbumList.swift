//
//  User.swift
//  music-playlist
//
//  Created by Igor Postoev on 10.10.2021.
//

enum AlbumDecodingKeys: String, CodingKey {
    case results
    case albummatches
    case album
    case name
    case artist
    case image
    case size
    case text = "#text"
}

struct AlbumList: Decodable {
    var albums: [Album]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: AlbumDecodingKeys.self)
        let results = try container.nestedContainer(keyedBy: AlbumDecodingKeys.self, forKey: .results)
        let albummatches = try results.nestedContainer(keyedBy: AlbumDecodingKeys.self, forKey: .albummatches)
        albums = try albummatches.decode([Album].self, forKey: .album)
    }
}

struct Album: Decodable {
    var name: String?
    var artist: String?
    var images: [AlbumImage]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: AlbumDecodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        artist = try container.decode(String.self, forKey: .artist)
        images = try container.decode([AlbumImage].self, forKey: .image)
    }
}

struct AlbumImage: Decodable {
    var text: String
    var size: String
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: AlbumDecodingKeys.self)
        text = try container.decode(String.self, forKey: .text)
        size = try container.decode(String.self, forKey: .size)
    }
}

enum ImageSize: String {
    case small
    case medium
    case large
}
