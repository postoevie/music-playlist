//
//  ViewController.swift
//  music-playlist
//
//  Created by Igor Postoev on 10.10.2021.
//

import UIKit

class ViewController: UIViewController,
                      UITableViewDelegate,
                      UITableViewDataSource,
                      UISearchBarDelegate {

    @IBOutlet weak var table: UITableView!
    private let searchController = UISearchController()
    
    var items = [Album]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        populateItems(searchText: "believe")
    }
    
    private func populateItems(searchText: String) {
        AlbumFetcher.performRequest(searchText: searchText) { [weak self] result in
            switch result {
            case .success(let response):
                self?.items = response.albums ?? []
            case .failure(let error):
                self?.items = []
                print(error)
            }
            self?.table?.reloadData()
        }
    }
    
    private func setupSubviews() {
        navigationItem.searchController = searchController
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.hidesSearchBarWhenScrolling = false
        table.delegate = self
        table.dataSource = self
    }

    // MARK: -UITableViewDelegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let album = items[indexPath.row]
        cell.textLabel?.text = album.name
        cell.detailTextLabel?.text = album.artist
        let image = album.images?.first {
            $0.size == ImageSize.small.rawValue
        }
        let imageCompletion: (UIImage) -> Void = { (image: UIImage) in
            cell.imageView?.image = image
        }
        cell.imageView?.image = UIImage(named: "placeholder")
        let imagePath = image?.text ?? ""
        ImageFetcher.performRequest(urlPath: imagePath, imageCompletion)
        return cell
    }
    
    // MARK: -UISearchBarDelegate
    func searchBar(_ searchBar: UISearchBar,
                   textDidChange searchText: String) {
        let timer = Timer(timeInterval: 2,
                   repeats: false) { [weak self] _ in
            self?.populateItems(searchText: searchText)
        }
        RunLoop.current.add(timer, forMode: .common)
    }
}

