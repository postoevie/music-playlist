//
//  Throwable.swift
//  music-playlist
//
//  Created by Igor Postoev on 11.10.2021.
//

public struct Throwable<T: Decodable>: Decodable {
    
    public let result: Result<T, Error>

    public init(from decoder: Decoder) throws {
        let catching = { try T(from: decoder) }
        result = Result(catching: catching )
    }
}
