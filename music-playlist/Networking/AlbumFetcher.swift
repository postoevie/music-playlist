//
//  AlbumNetworking.swift
//  music-playlist
//
//  Created by Igor Postoev on 11.10.2021.
//

import Foundation

class AlbumFetcher {
    
    static func performRequest(searchText: String,
                               _ completion: @escaping (Result<AlbumList, Error>) -> Void) {
        let reqPath = "https://ws.audioscrobbler.com/2.0/?method=album.search&album=\(searchText)&api_key=5e92cd0dbaaa927d2b4c4fb9f8b2bccc&format=json&limit=50"
        let request = NSURLRequest(url: NSURL(string: reqPath)! as URL)
        
        let queue = DispatchQueue(label: "images download",
                                  qos: .userInteractive)
        queue.async {
            let handler = { (data: Data?,
                             response: URLResponse?,
                             error: Error?) in
                if let responseError = error {
                    completion(.failure(responseError))
                    return
                }
                if let httpResponse = response as? HTTPURLResponse,
                   httpResponse.statusCode >= 400 {
                    let error = NSError(domain: "",
                                        code: httpResponse.statusCode,
                                        userInfo: nil)
                    completion(.failure(error))
                }
                if let recievedData = data,
                   let albumList = try? JSONDecoder().decode(AlbumList.self, from: recievedData) {
                    DispatchQueue.main.async {
                        completion(.success(albumList))
                    }
                }
            }
            let dataTask = URLSession.shared.dataTask(with: request as URLRequest,
                                                      completionHandler: handler)
            dataTask.resume()
        }
    }
}
