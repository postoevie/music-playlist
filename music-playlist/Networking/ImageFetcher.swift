//
//  ImageFetcher.swift
//  music-playlist
//
//  Created by Igor Postoev on 13.10.2021.
//

import UIKit

class ImageFetcher {
    
    static func performRequest(urlPath: String, _ completion: @escaping (UIImage) -> Void) {
        let queue = DispatchQueue(label: "images download",
                                  qos: .userInteractive)
        queue.async {
            guard let url = NSURL(string: urlPath) as URL?,
                  let data = try? Data(contentsOf: url),
                  let image = UIImage(data: data) else {
                return
            }
            DispatchQueue.main.async {
                completion(image)
            }
        }
    }
}
